# 64-bit integers in Coq

```coq
From Int64 Require Import Int64. (* Signed *)
From Int64 Require Import UInt64. (* Unsigned *)
```
