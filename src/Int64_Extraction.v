(* begin hide *)
From Int64 Require Import Int64_Definitions.
From Coq.extraction Require Import Extraction.

Extraction Blacklist Int64.
(* end hide *)

Extract Inductive Int64.t => "Stdlib.Int64.t"
  [ "(fun (sign,mag) ->
       let x = Stdlib.Int64.of_int (Obj.magic mag) in
       if sign then Stdlib.Int64.logor x Stdlib.Int64.min_int else Stdlib.Int64.logand x Stdlib.Int64.max_int)" ]
  "(fun go x -> go (x < 0L) (Obj.magic (Stdlib.Int64.to_int x)))".
Extract Inlined Constant Int64.add => "Stdlib.Int64.add".
Extract Inlined Constant Int64.sub => "Stdlib.Int64.sub".
Extract Inlined Constant Int64.mul => "Stdlib.Int64.mul".
Extract Inlined Constant Int64.land_ => "Stdlib.Int64.logand".
Extract Inlined Constant Int64.lor_ => "Stdlib.Int64.logor".
Extract Inlined Constant Int64.lxor_ => "Stdlib.Int64.logxor".
Extract Inlined Constant Int64.lsr => "(fun x y -> let y : int = Obj.magic y in if y >= 0 && y <= 63 then Stdlib.Int64.shift_right_logical x y else assert false)". (* TODO: Be careful aboud unsignedness *)
Extract Inlined Constant Int64.lsl => "(fun x y -> let y : int = Obj.magic y in if y >= 0 && y <= 63 then Stdlib.Int64.shift_left x y else assert false)". (* TODO: Be careful about unsignedness *)
Extract Inlined Constant Int64.eqb => "((=) : Stdlib.Int64.t -> Stdlib.Int64.t -> bool)".
Extract Inlined Constant Int64.compare => "Stdlib.Int64.compare".
Extract Inlined Constant Int64.ltb => "((<) : Stdlib.Int64.t -> Stdlib.Int64.t -> bool)".
Extract Inlined Constant Int64.of_int63 => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.of_Z => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.of_N => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.of_hex => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.to_int => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.to_Z => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.of_N => "Stdlib.Int64.add".

Extract Inlined Constant Int64.div => "(fun _ _ -> assert false)". (* wrong type of arguments *)
Extract Inlined Constant Int64.mod_ => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.divmod => "(fun _ _ -> assert false)".
Extract Inlined Constant Int64.of_bytes => "(fun _ _ -> assert false)".
