(* begin hide *)
From Coq Require Import
  Int63.

From Int64 Require Import
  UInt64_Definitions.

Local Open Scope uint64.
Set Primitive Projections.
(* end hide *)

Lemma add_comm (a b : uint64) : a + b = b + a.
Proof.
  unfold UInt64.add.
  destruct a as [ ? [] ], b as [ ? [] ]; f_equal.
  all: try apply Int63.add_comm.
  f_equal; apply Int63.add_comm.
Qed.

Lemma add_assoc (a b c : uint64) : (a + (b + c) = (a + b) + c)%uint64.
Proof.
  intros. unfold UInt64.add.
  destruct a as [ ? []], b as [ ? []], c as [ ? []]; f_equal.
  all: rewrite ! Int63.add_assoc, ? (Int63.add_comm _ 1), ? Int63.add_assoc.
  all: reflexivity.
Qed.
