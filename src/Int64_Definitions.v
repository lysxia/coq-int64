(** * 64-bit signed integers *)

(* begin hide *)
From Coq Require Ascii String HexString.
From Coq Require Import
  ZArith Int63.

Set Primitive Projections.
(* end hide *)

Module Int64.

(** 64-bit signed integers *)
Record t : Set := Int64 {
  sign : bool; (* Sign bit: negative if true *)
  magnitude63 : int; (* 63 bit magnitude *)
}.

Local Open Scope int63.

Definition size : nat := 64.

(** ** Arithmetic *)

Definition add (x y : t) : t :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  let s := m + n in
  let b := xorb (xorb i j) (s < m)%int63 in
  Int64 b s.

Definition sub (x y : t) : t :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  let s := m - n in
  let b := xorb (xorb i j) (m < s)%int63 in
  Int64 b s.

Definition mul (x y : t) : t :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  let mn := (m * n) in
  let p1 := (m * (lsr n 1)) in
  let p2 := p1 + (lsr (m * (n land 1)) 1) in
  let b := xorb (xorb (i && Int63.bit n 0) (j && Int63.bit m 0)) (Int63.bit p2 62) in
  Int64 b mn.

Definition land_ (x y : t) : t :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  Int64 (andb i j) (m land n).

Definition lor_ (x y : t) : t :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  Int64 (orb i j) (m lor n).

Definition lxor_ (x y : t) : t :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  Int64 (xorb i j) (m lxor n).

Definition lsr (x : t) (n : int) : t :=
  if (0 < n)%int63 then
    let '(Int64 i m) := x in
    let m' :=
      if i then (m >> n) lor (1 << (Int63.digits - n)) else (m >> n) in
    Int64 false m'
  else x.

Definition lsl (x : t) (n : int) : t :=
  if (0 < n)%int63 then
    let '(Int64 i m) := x in
    let b := Int63.get_digit m (Int63.digits - n) in
    Int64 b (m << n)
  else x.

(** ** Comparison *)

Definition eqb (x y : t) : bool :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  (bool_eq i j && Int63.eqb m n)%bool.

Definition compare_bool (i j : bool) : comparison :=
  match i, j with
  | true, true | false, false => Eq
  | true, false => Gt
  | false, true => Lt
  end.

Definition compare (x y : t) : comparison :=
  let '(Int64 i m) := x in
  let '(Int64 j n) := y in
  match compare_bool i j with
  | Eq => Int63.compare m n
  | Lt => Lt
  | Gt => Gt
  end.

Definition ltb (m n : t) : bool :=
  match compare m n with
  | Lt => true
  | _ => false
  end.

(** ** Conversions *)

Import Ascii String HexString.

(** See [of_hex] *)
Local Fixpoint _filterhex (s : string) : string :=
  match s with
  | ""%string => ""
  | String x xs =>
    match x with
    | "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
    | "a" | "b" | "c" | "d" | "e" | "f"
    | "A" | "B" | "C" | "D" | "E" | "F" => String x (_filterhex xs)
    | _ => _filterhex xs
    end%char
  end.

(** *** To [int64] *)

Definition of_int63 (n : int) : t :=
  Int64 false n.

Definition of_Z (n : Z) : t :=
  Int64 (Z.testbit n 63) (Int63.of_Z n).

Definition of_N (n : N) : t :=
  Int64 (N.testbit n 63) (Int63.of_Z (Z.of_N n)).

(** Convert a hex string to a 64-bit int.

  Separators [_] are allowed for readability: ["DEAD_BEEF"].
 *)
(* TODO: forbid bad characters *)
Definition of_hex (s : string) : t :=
  of_N (HexString.Raw.to_N (_filterhex s) 0).

(** *** From [int64] *)

(** Truncates sign. Note: [to_int (-1) = Int63.max_int]. *)
Definition to_int (x : t) : int :=
  magnitude63 x.

Definition Z_2_63 : Z := Eval compute in 2 ^ 63.

Definition to_Z (n : t) : Z :=
   let y := Int63.to_Z (magnitude63 n) in
   if sign n then y - Z_2_63 else y.

(** ** More arithmetic *)

(* TODO: this is broken if the divisor is negative *)
Definition divmod (x : t) (n : int) : t * int :=
  let '(Int64 i m) := x in
  if i then
    (Int64 false 0, 0)
  else
    (Int64 false (Int63.div m n), Int63.mod m n).

Definition div (m : t) (n : int) : t := fst (divmod m n).
Definition mod_ (m : t) (n : int) : int := snd (divmod m n).

Local Definition _print_byte_dummy : t -> option Byte.byte := fun _ => None.
Definition of_bytes (s : list Byte.byte) : t :=
  Int64.of_hex (string_of_list_byte s).

End Int64.

(** ** Notations *)

Notation int64 := Int64.t.

(** *** Notation scope *)

Declare Scope int64_scope.
Bind Scope int64_scope with Int64.t.
Delimit Scope int64_scope with int64.

(** *** Infix operators *)

Infix "+" := Int64.add : int64_scope.
Infix "-" := Int64.sub : int64_scope.
Infix "*" := Int64.mul : int64_scope.
Infix "<<" := Int64.lsl : int64_scope.
Infix ">>" := Int64.lsr : int64_scope.
Infix "land" := Int64.land_ : int64_scope.
Infix "lor" := Int64.lor_ : int64_scope.
Infix "lxor" := Int64.lxor_ : int64_scope.
Infix "mod" := Int64.mod_ : int64_scope.
Infix "=?" := Int64.eqb : int64_scope.
Infix "<?" := Int64.ltb : int64_scope.

(** *** Literals *)

Declare Scope int64_hex_scope.
Delimit Scope int64_hex_scope with int64_hex.

(** NB: Literals are evaluated, as opposed to plain function applications. *)
Numeral Notation Int64.t Int64.of_Z Int64.to_Z
  : int64_scope.

String Notation Int64.t Int64.of_bytes Int64._print_byte_dummy
  : int64_hex_scope.

