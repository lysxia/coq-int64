(** * 64-bit unsigned integers *)

(* begin hide *)
From Coq Require Ascii String HexString.
From Coq Require Import
  ZArith Int63.

Set Primitive Projections.
(* end hide *)

Module UInt64.

(** 64-bit unsigned integers *)
Record t : Set := UInt64 {
  ms63b : int; (* Most significant 63 bits *)
  lsb : bool;  (* Least significant bit *)
}.

Local Open Scope int63.

Definition size : nat := 64.

(** ** Arithmetic *)

Definition add (x y : t) : t :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  match i, j with
  | true, true => UInt64 (m + n + 1) false
  | false, i | i, false => UInt64 (m + n) i
  end.

Definition sub (x y : t) : t :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  match i, j with
  | true, true | false, false => UInt64 (m - n) false
  | true, false => UInt64 (m - n) true
  | false, true => UInt64 (m - n - 1) true
  end.

Definition mul (x y : t) : t :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  let mn := (m * n) * 2 in
  let mj := if j then m else 0 in
  let ni := if i then n else 0 in
  UInt64 (mn + mj + ni) (andb i j).

Axiom signed_mul : t -> t -> t.

Definition land_ (x y : t) : t :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  UInt64 (m land n) (andb i j).

Definition lor_ (x y : t) : t :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  UInt64 (m lor n) (orb i j).

Definition lxor_ (x y : t) : t :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  UInt64 (m lxor n) (xorb i j).

Definition lsr (x : t) (n : int) : t :=
  let '(UInt64 m i) := x in
  if (0 < n)%int63 then
    UInt64 (m >> n) (get_digit m (n - 1))
  else
    UInt64 m i.

Definition lsl (x : t) (n : int) : t :=
  let '(UInt64 m i) := x in
  if (0 < n)%int63 then
    UInt64 ((m << n) lor (b2i i << (n - 1))) false
  else
    UInt64 m i.

(** ** Comparison *)

Definition eqb (x y : t) : bool :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  (Int63.eqb m n && bool_eq i j)%bool.

Definition compare_bool (i j : bool) : comparison :=
  match i, j with
  | true, true | false, false => Eq
  | true, false => Gt
  | false, true => Lt
  end.

Definition compare (x y : t) : comparison :=
  let '(UInt64 m i) := x in
  let '(UInt64 n j) := y in
  match Int63.compare m n with
  | Eq => compare_bool i j
  | Lt => Lt
  | Gt => Gt
  end.

Definition ltb (m n : t) : bool :=
  match compare m n with
  | Lt => true
  | _ => false
  end.

(** ** Conversions *)

Import Ascii String HexString.

(* Auxiliary *)
Local Fixpoint _filterhex (s : string) : string :=
  match s with
  | ""%string => ""
  | String x xs =>
    match x with
    | "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
    | "a" | "b" | "c" | "d" | "e" | "f"
    | "A" | "B" | "C" | "D" | "E" | "F" => String x (_filterhex xs)
    | _ => _filterhex xs
    end%char
  end.

(** *** To [uint64] *)

Definition of_int (n : int) : t :=
  UInt64 (n >> 1) (bit n 0).

Definition of_Z (n : Z) : t :=
  UInt64 (Int63.of_Z (Z.div2 n)) (Z.testbit n 0).

Definition of_N (n : N) : t :=
  UInt64 (Int63.of_Z (Z.of_N (N.div2 n))) (N.testbit n 0).

Definition of_hex (s : string) : t :=
  of_N (HexString.Raw.to_N (_filterhex s) 0).

(** *** From [uint64] *)

(** Truncates *)
Definition to_int (x : t) : int :=
  let '(UInt64 m i) := x in
  (m << 1) + b2i i.

Definition to_Z (n : t) : Z :=
  2 * Int63.to_Z (ms63b n) + Z.b2z (lsb n).

(** ** More arithmetic *)

Fixpoint slow_div (qmax : nat) (m n : t) : t :=
  if ltb m n then UInt64 0 false
  else
    match qmax with
    | O => UInt64 0 true
    | S qmax => add (UInt64 0 true) (slow_div qmax (sub m n) n)
    end.

Definition divmod (x : t) (n : int) : t * int :=
  let '(UInt64 m i) := x in
  let q := Int63.div m n in
  let r := Int63.mod m n in
  let q' := (Int63.ltb (n >> 1) r || (Int63.eqb (n >> 1) r && implb (bit n 0) i))%bool in
  (UInt64 q q', if q' then (r << 1) + b2i i - n else (r << 1) + b2i i).

Definition div (m : t) (n : int) : t := fst (divmod m n).
Definition mod_ (m : t) (n : int) : int := snd (divmod m n).

Local Definition _print_byte_dummy : t -> option Byte.byte := fun _ => None.
Definition of_bytes (s : list Byte.byte) : t :=
  UInt64.of_hex (string_of_list_byte s).

End UInt64.

(** ** Notations *)

Notation uint64 := UInt64.t.

(** *** Notation scope *)

Declare Scope uint64_scope.
Bind Scope uint64_scope with UInt64.t.
Delimit Scope uint64_scope with uint64.

(** *** Infix operators *)

Infix "+" := UInt64.add : uint64_scope.
Infix "-" := UInt64.sub : uint64_scope.
Infix "*" := UInt64.mul : uint64_scope.
Infix "<<" := UInt64.lsl : uint64_scope.
Infix ">>" := UInt64.lsr : uint64_scope.
Infix "land" := UInt64.land_ : uint64_scope.
Infix "lor" := UInt64.lor_ : uint64_scope.
Infix "lxor" := UInt64.lxor_ : uint64_scope.
Infix "mod" := UInt64.mod_ : uint64_scope.
Infix "=?" := UInt64.eqb : uint64_scope.
Infix "<?" := UInt64.ltb : uint64_scope.

(** *** Literals *)

Declare Scope uint64_hex_scope.
Delimit Scope uint64_hex_scope with uint64_hex.

(** NB: Literals are evaluated, as opposed to plain function applications. *)
Numeral Notation UInt64.t UInt64.of_Z UInt64.to_Z
  : uint64_scope.

String Notation UInt64.t UInt64.of_bytes UInt64._print_byte_dummy
  : uint64_hex_scope.
