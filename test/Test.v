From Coq Require Import Int63.
From Int64 Require Import UInt64.

Section UnitTest.

Local Open Scope uint64.

Local Infix "=" := (@eq uint64).

Let add_2_2 : 2 + 2 = 4 := eq_refl.
Let add_3_2 : 3 + 2 = 5 := eq_refl.
Let add_2_3 : 2 + 3 = 5 := eq_refl.
Let add_3_3 : 3 + 3 = 6 := eq_refl.

Let sub_4_2 : 4 - 2 = 2 := eq_refl.
Let sub_5_2 : 5 - 2 = 3 := eq_refl.
Let sub_5_3 : 5 - 3 = 2 := eq_refl.
Let sub_6_3 : 6 - 3 = 3 := eq_refl.

Let mul_2_2 : 2 * 2 = 4 := eq_refl.
Let mul_3_2 : 3 * 2 = 6 := eq_refl.
Let mul_2_3 : 2 * 3 = 6 := eq_refl.
Let mul_3_3 : 3 * 3 = 9 := eq_refl.

Let lsr_2_1 : 2 >> 1 = 1 := eq_refl.
Let lsr_3_1 : 3 >> 1 = 1 := eq_refl.
Let lsr_5_1 : 5 >> 1 = 2 := eq_refl.
Let lsr_5_2 : 5 >> 2 = 1 := eq_refl.

Let lsl_2_1 : 2 << 1 = 4 := eq_refl.
Let lsl_3_1 : 3 << 1 = 6 := eq_refl.

Let div_2_2 : UInt64.divmod 2 2 = (1, 0%int63) := eq_refl.
Let div_2_3 : UInt64.divmod 2 3 = (0, 2%int63) := eq_refl.
Let div_3_2 : UInt64.divmod 3 2 = (1, 1%int63) := eq_refl.
Let div_4_2 : UInt64.divmod 4 2 = (2, 0%int63) := eq_refl.
Let div_5_2 : UInt64.divmod 5 2 = (2, 1%int63) := eq_refl.

Let of_hex_FF : "FF"%uint64_hex = 255 := eq_refl.

End UnitTest.
(* end hide *)
